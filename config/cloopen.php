<?php

return [
    // +----------------------------------------------------------------------
    // | 容云短信对接配置信息
    // +----------------------------------------------------------------------
    'template_id' => 12334,//填写短信验证码模板id
    'expires' => 1,//填写短信验证码失效分钟数，如3分钟过期就填3
    'server_ip' => 'app.cloopen.com',
    'server_port' => '8883',
    'soft_version' => '2013-12-26',
    'account_sid' => 'xxxx',
    'account_token' => 'xxxxxx',
    'app_id' => 'xxxxx',
    'app_redis_cache_db_number' => 1,//缓存到redis的DB编号
    'app_redis_cache_key_prefix' => 'cloopen:',//缓存到redis时所有key的前缀
    'enable_log' => true//日志
];
