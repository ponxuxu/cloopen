<?php

namespace Cloopen;

use Cloopen\Exceptions\CaptchaException;
use Illuminate\Support\Facades\Http;

class SmsService
{
    private int $templateId;

    private string $appId;
    private string $accountSid;

    private string $accountToken;

    private string $serverIp;
    private int $serverPort;

    private string $softVersion;


    /**
     * @throws CaptchaException
     */
    public function __construct(array $config = [])
    {
        $this->setTemplateId($config['template_id'] ?? 0);
        $this->setAppId($config['app_id'] ?? '');
        $this->setAccountSid($config['account_sid'] ?? '');
        $this->setAccountToken($config['account_token'] ?? '');
        $this->setServerIp($config['server_ip'] ?? '');
        $this->setServerPort($config['server_port'] ?? 0);
        $this->setSoftVersion($config['soft_version'] ?? '');
    }

    public function getTemplateId(): int
    {
        return $this->templateId;
    }

    public function setTemplateId(int $templateId): static
    {
        if (empty($templateId)) {
            throw new CaptchaException('请设置 template_id 参数');
        }
        $this->templateId = $templateId;

        return $this;
    }

    public function getAppId(): string
    {
        return $this->appId;
    }

    public function setAppId(string $appId): static
    {
        if (empty($appId)) {
            throw new CaptchaException('请设置 app_id 参数');
        }

        $this->appId = $appId;

        return $this;
    }

    public function getAccountSid(): string
    {
        return $this->accountSid;
    }

    public function setAccountSid(string $accountSid): static
    {
        if (empty($accountSid)) {
            throw new CaptchaException('请设置 account_sid 参数');
        }

        $this->accountSid = $accountSid;

        return $this;
    }

    public function getAccountToken(): string
    {
        return $this->accountToken;
    }

    public function setAccountToken(string $accountToken): static
    {

        if (empty($accountToken)) {
            throw new CaptchaException('请设置 account_token 参数');
        }

        $this->accountToken = $accountToken;

        return $this;
    }

    // Repeat the pattern for other properties...

    // Getter and setter for serverIp
    public function getServerIp(): string
    {
        return $this->serverIp;
    }

    public function setServerIp(string $serverIp): static
    {
        if (empty($serverIp)) {
            throw new CaptchaException('请设置 server_id 参数');
        }

        $this->serverIp = $serverIp;

        return $this;
    }

    public function getServerPort(): int
    {
        return $this->serverPort;
    }

    public function setServerPort(int $serverPort): static
    {
        if (empty($serverPort)) {
            throw new CaptchaException('请设置 server_port 参数');
        }

        $this->serverPort = $serverPort;

        return $this;
    }

    public function getSoftVersion(): string
    {
        return $this->softVersion;
    }

    public function setSoftVersion(string $softVersion): static
    {
        if (empty($softVersion)) {
            throw new CaptchaException('请设置 soft_version 参数');
        }

        $this->softVersion = $softVersion;

        return $this;
    }

    /**
     * 生成sig
     */
    protected function generateSig($batch): string
    {
        return strtoupper(md5($this->getAccountSid() . $this->getAccountToken() . $batch));
    }

    /**
     * 生成请求url
     */
    protected function generateUrl(string $sig): string
    {
        return 'https://' . $this->getServerIp() . ':' . $this->getServerPort() . '/' . $this->getSoftVersion() . '/Accounts/' . $this->getAccountSid() . '/SMS/TemplateSMS?sig=' . $sig;
    }

    /**
     * 生成请求头
     */
    protected function generateHeader($batch): array
    {
        //生成授权：主帐户Id + 英文冒号 + 时间戳。
        $authorization = base64_encode($this->accountSid . ":" . $batch);

        return [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json;charset=utf-8',
            'Authorization' => $authorization
        ];
    }

    /**
     * 生成请求体
     */
    public function send(array $mobiles, array $datas, $reqId = null, $subAppend = null): array
    {
        $data = [
            'to' => implode(',', $mobiles),
            'datas' => $datas,
            'appId' => $this->getAppId(),
            'templateId' => $this->getTemplateId(),
        ];

        if (!empty($reqId)) {
            $data['reqId'] = $reqId;
        }

        if (!empty($subAppend)) {
            $data['subAppend'] = $subAppend;
        }

        $batch = date('YmdHis');
        $url = $this->generateUrl($this->generateSig($batch));
        $headers = $this->generateHeader($batch);
        $result = Http::withHeaders($headers)->post($url, $data)->json();

        if (empty($result['statusCode']) || $result['statusCode'] != '000000') {
            $errMsg = $result['statusMsg'] ?? '短信发送失败';
            throw new CaptchaException($errMsg);
        }

        return [
            'sms_message_sid' => $result['templateSMS']['smsMessageSid'] ?? '',
            'date_created' => $result['templateSMS']['dateCreated'] ?? ''
        ];
    }
}
