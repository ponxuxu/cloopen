<?php

namespace Cloopen;

use Cloopen\Exceptions\CaptchaException;
use Cloopen\Exceptions\ServerDisposeException;
use Illuminate\Support\Facades\Redis;

class CloopenService
{
    /**
     * @var array 配置
     */
    private array $config;

    /**
     * @var int|null
     */

    private ?int $captcha = null;

    /**
     * @var string 手机号
     */
    private string $mobile;

    /**
     * SmsCaptcha constructor.
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        $config = config('cloopen');
        $options = array_merge($config, $options);
        $options['template_id'] = $options['template_id'] ?? 0;
        $options['scene_id'] = $options['scene_id'] ?? 'default';
        $options['captcha'] = $options['captcha'] ?? 0;
        $options['expires'] = $options['expires'] ?? 3;
        $options['interval'] = $options['interval'] ?? 60;
        $options['cache_key_prefix'] = $options['cache_key_prefix'] ?? 'cloopen:';
        $options['mobile_daily_send_max'] = $options['mobile_daily_send_max'] ?? 1000;
        //  $this->appRedisCacheDbNumber = $options['app_redis_cache_db_number'] ?? 1;

        $this->config = $options;
    }

    public function getConfig(string $key = null): array|string|int
    {
        if (!is_null($key) && isset($this->config[$key])) {
            return $this->config[$key];
        }

        return $this->config;
    }

    public function setConfig(array $config): static
    {
        $this->config = array_merge($this->config, $config);

        return $this;
    }

    /**
     * 设置场景ID
     * @param int|string $sceneId
     * @return $this
     */
    public function setSceneId(int|string $sceneId): static
    {
        $this->setConfig(['scene_id' => $sceneId]);

        return $this;
    }

    public function getSceneId(): string
    {
        return $this->getConfig('scene_id');
    }

    /**
     * 设置模板ID
     * @param $templateId
     * @return $this
     */
    public function setTemplateId($templateId): static
    {
        $this->setConfig(['template_id' => $templateId]);

        return $this;
    }

    public function getTemplateId(): string
    {
        return $this->getConfig('template_id');
    }

    /**
     * 设置验证码
     * @param int $captcha
     * @return $this
     */
    public function setCaptcha(int $captcha): static
    {
        $this->captcha = $captcha;

        return $this;
    }


    public function getCaptcha(): ?int
    {
        return $this->captcha;
    }

    /**
     * 设置有效时长（分钟）
     * @param int $minutes
     * @return $this
     */
    public function setExpires(int $minutes): static
    {
        $this->setConfig(['expires' => $minutes]);

        return $this;
    }

    public function getExpires(): int
    {
        return $this->getConfig('expires');
    }


    public function setCacheKeyPrefix(string $prefix): static
    {
        $this->setConfig(['cache_key_prefix' => $prefix]);

        return $this;
    }

    public function getCacheKeyPrefix(): string
    {
        return $this->getConfig('cache_key_prefix');
    }

    /**
     * 设置间隔时长（秒）
     * @param int $seconds
     * @return $this
     */
    public function setInterval(int $seconds): static
    {
        $this->setConfig(['interval' => $seconds]);

        return $this;
    }

    public function getInterval(): int
    {
        return $this->getConfig('interval');
    }

    /**
     * 设置手机号码
     * @param string $mobile
     * @return $this
     */
    public function setMobile(string $mobile): static
    {
        $this->mobile = $mobile;

        return $this;
    }

    public function getMobile(): string
    {
        return $this->mobile;
    }

    /**
     * 单一手机号每日发送上限
     * @param $mobileDailySendMax
     * @return $this
     */
    public function setMobileDailySendMax($mobileDailySendMax): static
    {
        $this->setConfig(['mobile_daily_send_max' => $mobileDailySendMax]);

        return $this;
    }

    public function getMobileDailySendMax()
    {
        return $this->config['mobile_daily_send_max'];
    }

    /**
     * 生成随机验证码
     * @param int $length 验证码长度
     */
    private function createCaptcha(int $length = 6): int
    {
        for ($captcha = '', $i = 0; $i < $length; $i++) {
            $min = empty($captcha) ? 1 : 0;
            $captcha .= mt_rand($min, 9);
        }
        $this->setCaptcha($captcha);

        return $captcha;
    }


    /**
     * 发送短信验证码
     * @param int $length
     * @return true
     * @throws CaptchaException
     */
    public function send(int $length = 6): true
    {
        //验证配置参数
        $config = $this->getSendConfig();

        $ttl = $this->getMobileTtl();
        if ($ttl > 0) {
            throw new CaptchaException('短信验证码获取过于频繁，请' . $ttl . '秒后再试', 410016);
        }

        if ($this->isSendLimitExceeded()) {
            throw new CaptchaException('短信验证码获取次数已到达上限', 410017);
        }

        $this->createCaptcha($length);

        $sms = new SmsService($config);
        $sms->send([$this->getMobile()], [$this->getCaptcha(), $this->getExpires() . '分钟']);

        if ($this->getInterval() > 0) {
            Redis::setex($this->getKey() . ':interval_lock', $this->getInterval(), $this->getCaptcha());
        }

        return Redis::setex($this->getKey(), $this->getExpires() * 60, $this->getCaptcha());
    }


    /**
     * 检查验证码是否正确
     * @throws CaptchaException|ServerDisposeException
     */
    public function check(int $captcha = null, string $mobile = null, string $sceneId = null)
    {

        $this->setCaptcha($captcha ?? $this->getCaptcha());
        if (empty($this->getCaptcha())) {
            throw new CaptchaException('未获取到验证码');
        }

        $this->setMobile($mobile ?? $this->getMobile());
        if (empty($this->getMobile())) {
            throw new CaptchaException('未获取到手机号');
        }

        $this->setSceneId($sceneId ?? $this->getSceneId());
        if (empty($this->getSceneId())) {
            throw new CaptchaException('请设置场景ID');
        }

        if (Redis::get($this->getKey()) == $this->getCaptcha()) {
            return Redis::del($this->getKey());
        }

        throw new CaptchaException('短信验证码错误');
    }
    /**
     * @desc  获取发送配置信息
     * @return array
     * @throws CaptchaException
     */
    private function getSendConfig(): array
    {
        if (empty($this->getSceneId())) {
            throw new CaptchaException('请设置场景ID', 510012);
        }
        if (empty($this->getTemplateId())) {
            throw new CaptchaException('请设置模板ID', 510013);
        }
//        if (empty($this->getCaptcha())) {
//            throw new CaptchaException('请设置验证码', 510014);
//        }
        if (empty($this->getMobile())) {
            throw new CaptchaException('请设置接收手机号码', 510015);
        }

        return $this->config;
    }

    /**
     * @desc 获取手机号限制时间
     * @return int
     */
    private function getMobileTtl(): int
    {
        return Redis::ttl($this->getKey() . ':interval_lock');
    }

    /**
     * @desc 手机号是否超出当日接收短信最大次数
     * @return bool
     */
    private function isSendLimitExceeded(): bool
    {
        if ($this->getMobileDailySendMax() <= 0) {
            return false;
        }

        $cacheKey = $this->getKey() . ':' . date('Ymd') . ':send';
        $sendCount = Redis::incr($cacheKey);
        if ($sendCount == 1) {
            Redis::expire($cacheKey, $this->nowTillTomorrowMorningLeftSenconds());
        }

        if ($sendCount > $this->getMobileDailySendMax()) {
            return true;
        }

        return false;
    }

    /**
     * 获取验证码redis存储的key
     * @return string
     */
    private function getKey(): string
    {
        return $this->getCacheKeyPrefix() . $this->getMobile() . ':' . $this->getSceneId() . ':captcha';
    }

    /**
     * 现在到明天0点的秒数
     * @return int
     */
    private function nowTillTomorrowMorningLeftSenconds(): int
    {
        return strtotime('+1 day', strtotime(date('Y-m-d'))) - time();
    }

}
