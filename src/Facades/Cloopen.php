<?php

namespace Cloopen\Facades;

use Cloopen\CloopenService;
use Illuminate\Support\Facades\Facade;

/**
 * @method static setMobile(string $string)
 */
class Cloopen extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return CloopenService::class;
    }
}
