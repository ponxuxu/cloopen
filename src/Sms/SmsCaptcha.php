<?php

namespace Cloopen\Sms;

use Cloopen\Exceptions\ServerDisposeException;
use Illuminate\Support\Facades\Redis;

class SmsCaptcha
{
    /**
     * @var array 配置
     */
    private array $config;

    /**
     * @var string|int 场景ID
     */
    private string $sceneId;

    /**
     * @var string|int 模板ID
     */
    private int $templateId;

    /**
     * @var int 验证码
     */
    private int $captcha;

    /**
     * @var int 有效时长（分钟）
     */
    private int $expires;

    /**
     * @var int 间隔时常（秒）
     */
    private int $interval;


    /**
     * @var string|int 手机号码
     */
    private string $mobile;

    private string $appRedisCacheKeyPrefix;
    /**
     * @var int 单一手机号每日发送上限
     */
    private int $singleMobileDailySendMaximum;
    //  private int $appRedisCacheDbNumber;

    /**
     * SmsCaptcha constructor.
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        $config = config('cloopen');
        $options = array_merge($config, $options);

        $this->templateId = $options['template_id'] ?? 0;
        $this->sceneId = $options['scene_id'] ?? 'default';
        $this->captcha = $options['captcha'] ?? 0;
        $this->expires = $options['expires'] ?? 3;
        $this->interval = $options['interval'] ?? 60;
        $this->appRedisCacheKeyPrefix = $options['app_redis_cache_key_prefix'] ?? 'cloopen:';
        $this->singleMobileDailySendMaximum = $options['single_mobile_daily_send_maximum'] ?? 1000;
        //  $this->appRedisCacheDbNumber = $options['app_redis_cache_db_number'] ?? 1;

        $this->config = $options;
    }

    /**
     * 设置场景ID
     * @param int|string $sceneId
     * @return $this
     */
    public function setSceneId(int|string $sceneId): static
    {
        $this->sceneId = $sceneId;
        return $this;
    }

    /**
     * 设置模板ID
     * @param $templateId
     * @return $this
     */
    public function setTemplateId($templateId): static
    {
        $this->templateId = $templateId;
        return $this;
    }

    /**
     * 设置验证码
     * @param int $captcha
     * @return $this
     */
    public function setCaptcha(int $captcha): static
    {
        $this->captcha = $captcha;
        return $this;
    }

    /**
     * 生成随机验证码
     * @param int $length 验证码长度
     * @return $this
     */
    public function createCaptcha(int $length = 6): static
    {
        for ($captcha = '', $i = 0; $i < $length; $i++) {
            $min = empty($captcha) ? 1 : 0;
            $captcha .= mt_rand($min, 9);
        }
        return $this->setCaptcha($captcha);
    }

    /**
     * 设置有效时长（分钟）
     * @param int $minutes
     * @return $this
     */
    public function setExpires(int $minutes): static
    {
        $this->expires = $minutes;
        return $this;
    }

    /**
     * 设置间隔时长（秒）
     * @param int $seconds
     * @return $this
     */
    public function setInterval(int $seconds): static
    {
        $this->interval = $seconds;
        return $this;
    }

    /**
     * 设置手机号码
     * @param string $mobile
     * @return $this
     */
    public function setMobile(string $mobile): static
    {
        $this->mobile = $mobile;
        return $this;
    }

    /**
     * 单一手机号每日发送上限
     * @param $singleMobileDailySendMaximum
     * @return $this
     */
    public function setSingleMobileDailySendMaximum($singleMobileDailySendMaximum): static
    {
        $this->singleMobileDailySendMaximum = $singleMobileDailySendMaximum;
        return $this;
    }


    /**
     * @throws CaptchaException
     */
    public function validateConfig(): void
    {
        /**
         * 检测参数是否可用
         */
        if (empty($this->sceneId)) {
            throw new ServerDisposeException('请设置场景ID', 510012);
        }
        if (empty($this->templateId)) {
            throw new ServerDisposeException('请设置模板ID', 510013);
        }
        if (empty($this->captcha)) {
            throw new ServerDisposeException('请设置验证码', 510014);
        }
        if (empty($this->mobile)) {
            throw new ServerDisposeException('请设置接收手机号码', 510015);
        }
    }

    /**
     * 验证间隔时长
     * @throws CaptchaException
     */
    public function validateTtl(): void
    {
        $ttl = Redis::ttl($this->getKey() . ':interval_lock');
        if ($this->interval > 0 && $ttl !== -2) {
            throw new ServerDisposeException('短信验证码获取过于频繁，请' . $ttl . '秒后再试', 410016);
        }
    }

    /**
     * 单一手机号每日发送上限
     * @throws CaptchaException
     */
    public function validateSendNumber(): bool
    {
        if ($this->singleMobileDailySendMaximum <= 0) {
            return true;
        }

        $sendKey = $this->getKey() . ':' . date('Ymd') . ':send';
        $sendCount = Redis::incr($sendKey);
        if ($sendCount == 1) {
            Redis::expire($sendKey, $this->nowTillTomorrowMorningLeftSenconds());
        }

        if ($sendCount > $this->singleMobileDailySendMaximum) {
            throw new ServerDisposeException('短信验证码获取次数已到达上限', 410017);
        }

        return true;
    }


    /**
     * 发送短信验证码
     * @return true
     */
    public function send(): bool
    {
        $this->validateConfig();
        $this->validateTtl();
        $this->validateSendNumber();

        $this->config['template_id'] = $this->templateId;
        $sms = new Sms($this->config);

        $sms->send([$this->mobile], [$this->captcha, $this->expires . '分钟']);

        // 设置间隔时长
        if ($this->interval > 0) {
            Redis::setex($this->getKey() . ':interval_lock', $this->interval, $this->captcha);
        }

        //缓存验证码（用于效验）
        return Redis::setex($this->getKey(), $this->expires * 60, $this->captcha);
    }

    /**
     * 检查验证码是否正确
     * @throws CaptchaException
     */
    public function check(int $captcha = 0, string $mobile = '', string $sceneId = '')
    {
        $this->captcha = $captcha ?? $this->captcha;
        if (empty($this->captcha)) {
            throw new ServerDisposeException('未获取到验证码');
        }

        $this->mobile = $mobile ?? $this->mobile;
        if (empty($this->mobile)) {
            throw new ServerDisposeException('未获取到手机号');
        }

        $this->sceneId = $sceneId ?? $this->sceneId;
        if (empty($this->sceneId)) {
            throw new ServerDisposeException('请设置场景ID');
        }

        if (Redis::get($this->getKey()) == $captcha) {
            return Redis::del($this->getKey());
        }

        throw new ServerDisposeException('短信验证码错误');
    }

    /**
     * 获取验证码redis存储的key
     * @return string
     */
    private function getKey(): string
    {
        return $this->appRedisCacheKeyPrefix . $this->mobile . ':' . $this->sceneId . ':captcha';
    }

    /**
     * 现在到明天0点的秒数
     * @return int
     */
    public function nowTillTomorrowMorningLeftSenconds()
    {
        return strtotime('+1 day', strtotime(date('Y-m-d'))) - time();
    }


}
