<?php

namespace Cloopen;

use Illuminate\Support\ServiceProvider;

class CloopenServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/cloopen.php' => config_path('cloopen.php'), // 发布配置文件到 laravel 的config 下
        ]);
    }

    public function register()
    {
        $this->app->singleton('cloopen', function () {
            return new CloopenService();
        });
    }

}
