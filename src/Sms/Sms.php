<?php

namespace Cloopen\Sms;

use Cloopen\Exceptions\ServerDisposeException;
use Illuminate\Support\Facades\Http;

class Sms
{
    private int $templateId;

    private string $appId;
    private string $accountSid;

    private string $accountToken;

    private string $serverIp;
    private int $serverPort;

    private string $softVersion;


    public function __construct(array $config = [])
    {
        $this->templateId = $config['template_id'] ?? 0;
        $this->appId = $config['app_id'] ?? '';
        $this->accountSid = $config['account_sid'] ?? '';
        $this->accountToken = $config['account_token'] ?? '';
        $this->serverIp = $config['server_ip'] ?? '';
        $this->serverPort = $config['server_port'] ?? 0;
        $this->softVersion = $config['soft_version'] ?? '';
        $this->validateConfig();
    }

    /**
     * 验证配置
     */
    protected function validateConfig(): void
    {
        if (empty($this->templateId)) {
            throw new ServerDisposeException('template_id 未配置');
        }

        if (empty($this->appId)) {
            throw new ServerDisposeException('app_id 未配置');
        }

        if (empty($this->accountSid)) {
            throw new ServerDisposeException('account_sid 未配置');
        }

        if (empty($this->accountToken)) {
            throw new ServerDisposeException('account_token 未配置');
        }

        if (empty($this->serverIp)) {
            throw new ServerDisposeException('server_ip 未配置');
        }

        if (empty($this->serverPort)) {
            throw new ServerDisposeException('server_port 未配置');
        }

        if (empty($this->softVersion)) {
            throw new ServerDisposeException('soft_version 未配置');
        }
    }

    /**
     * 生成sig
     */
    protected function generateSig($batch): string
    {
        return strtoupper(md5($this->accountSid . $this->accountToken . $batch));
    }

    /**
     * 生成请求url
     */
    protected function generateUrl(string $sig): string
    {
        return 'https://' . $this->serverIp . ':' . $this->serverPort . '/' . $this->softVersion . '/Accounts/' . $this->accountSid . '/SMS/TemplateSMS?sig=' . $sig;
    }

    /**
     * 生成请求头
     */
    protected function generateHeader($batch): array
    {
        //生成授权：主帐户Id + 英文冒号 + 时间戳。
        $authorization = base64_encode($this->accountSid . ":" . $batch);

        return [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json;charset=utf-8',
            'Authorization' => $authorization
        ];
    }

    /**
     * 生成请求体
     */
    public function send(array $mobiles, array $datas, $reqId = null, $subAppend = null): array
    {
        $data = [
            'to' => implode(',', $mobiles),
            'datas' => $datas,
            'appId' => $this->appId,
            'templateId' => $this->templateId,
        ];

        if (!empty($reqId)) {
            $data['reqId'] = $reqId;
        }

        if (!empty($subAppend)) {
            $data['subAppend'] = $subAppend;
        }

        $batch = date('YmdHis');
        $url = $this->generateUrl($this->generateSig($batch));
        $headers = $this->generateHeader($batch);
        $result = Http::withHeaders($headers)->post($url, $data)->json();

        if (empty($result['statusCode']) || $result['statusCode'] != '000000') {
            $errMsg = $result['statusMsg'] ?? '短信发送失败';
            throw new ServerDisposeException($errMsg);
        }

        return [
            'sms_message_sid' => $result['templateSMS']['smsMessageSid'] ?? '',
            'date_created' => $result['templateSMS']['dateCreated'] ?? ''
        ];
    }
}
